# Exercices d’introduction à docker

Pré-requis : 
* [Installation de Docker](https://docs.docker.com/engine/install/)
* [Installation de Docker Compose](https://docs.docker.com/compose/install/)
* Installation de [Nginx](https://www.nginx.com/)
* Compréhension de la notion de [virtualisation](https://fr.wikipedia.org/wiki/Virtualisation)

## Exercice 1 : Première utilisation de docker

L'objectif de cet exercice est de prendre en main les premières commandes Docker. Nous allons créer un container (sorte de machine vituelle) puis de se connecter à l'intérieur pour explorer.

1. Utiliser `docker run ubuntu` pour démarrer un container avec une image du système d’exploitation [Ubuntu](https://hub.docker.com/_/ubuntu). Chercher le moyen dans les [options](https://docs.docker.com/engine/reference/commandline/run/) pour le garder en activité.

2. Trouver l' option pour démarrer le container en choisissant le nom, y mettre `prenom_exercice_1`.

3. Se connecter au container en utilisant `docker exec`. Les bonnes options sont dans la [documentation](https://docs.docker.com/engine/reference/commandline/exec/).

4. Une fois dans le container, installer wget ou curl et télécharger un gif dans le container. Il se peut que vous ayez quelques soucis, ce [lien](https://askubuntu.com/questions/1263284/apt-update-throws-signature-error-in-ubuntu-20-04-container-on-arm) peut être une solution.

5. Explorer le container et faire comme chez soi. Sortir du container avec `exit`.

## Exercice 2 : Volumes, le lien entre les systèmes de fichier du container et de l'hôte

1. Utiliser `docker run` pour démarrer un container avec une image du système d’exploitation [Alpine](https://alpinelinux.org/about/). Utiliser les options vu précédement pour démarrer le container en choisissant le nom, y mettre `prenom_exercice_2`.

2. Trouver l'option dans la [documentation](https://docs.docker.com/engine/reference/run/#volume-shared-filesystems) pour démarrer le container en montant le répertoire `~/entrainement/prenom/exercice2` du serveur sur le répertoire `/data` du container.

3. Se connecter au container en utilisant `docker exec`. Une fois dans le container, installer wget ou curl, puis télécharger un gif dans le container et le mettre dans `/data`.

4. Explorer le container et faire comme chez soi. Sortir du container avec `exit`.

5. Explorer les fichiers du serveur pour voir `~/entrainement/prenom/exercice2`. Confimer la présence du gif.

## 3. Objectifs : Utiliser le dockerfile pour créer une image

1. Sur le serveur, créer le répertoire `~/entrainement/prenom/exercice3`. Puis y créer un fichier portant le nom `Dockerfile`.

2. Créer un script pour télécharger un gif de votre choix dans le répertoire `/data` du qui s'exécutera dans le container.

3. Ecrire le `Dockerfile` en se basant sur la [documentation](https://docs.docker.com/engine/reference/builder/) pour créer une image basée sur `Ubuntu`, installer wget, et y importer votre script.

4. Construire votre image en lançant `docker build` avec les bonnes [options](https://docs.docker.com/engine/reference/commandline/build/) pour donner le nom `prenom_exercice` à l'image et la version 3.

5. Créer un container de cette image et monter le dossier `/data` sur `~/entrainement/prenom/exercice3` comme dans l'exercice précédent.

6. Explorer les fichiers du serveur pour voir `~/entrainement/prenom/exercice3`. Confimer la présence du gif.

## 4. Objectifs : Créer une image avec un fichier serveur

1. Dans le répertoire `~/entrainement/prenom/exercice4`, créer un nouveau `Dockerfile`.

2. Créer une image qui contient [Node JS](https://nodejs.org/en/).

3. Inclure dans l'image le script `server.js`. S'assurer que `server.js` est appelé à l'exécution de l'image. Ne pas oublier d'ajouter la commande `npm i` pour l'installation des dépendances **nodejs**.

4. Lancer un container de cette image avec `docker run`.

5. Pour connaitre l'ip de votre container taper `docker inspect` suivi du nom du container. En fin de description, trouver l'IP du container probablement proche de `"IPAddress": "172.17.0.X"`.

6. Depuis le server, utiliser curl pour faire une requete sur le server nodejs : `curl http://172.17.0.X:3000`

## 5. Objectifs : Utilisation de docker compose

Comme vous pouvez le voir, il y a parfois beaucoup de commande et d'options, il peut être laborieux de tout taper à la main. Heureusement, il y a [docker-compose](https://docs.docker.com/compose/) qui permet de gérer rapidement plusieurs containers en une seule ligne de commande.

1. Dans un répertoire, créer le dossier `~/entrainement/prenom/exercice5`. Y créer le fichier `docker-compose.yml`.

2. Se baser sur la [documentation](https://docs.docker.com/compose/compose-file/) pour créer le `docker-compose.yml`. Créer un service nommé app. Utilisé l'image que vous avez précédement créé.

3. Lancer `docker-compose up`. Tester votre server comme précedement.

4. Dans le `docker-compose.yml` ajouter une ligne pour modifier la variable d'environnement PORT pour mettre `3002`. Relancer `docker-compose up` puis tester à nouveau avec `curl http://172.17.0.X:3002`.

## 6. Objectifs : Utiliser le reverse proxy avec Nginx

Nous avons maintenant un service web qui répond localement à mes requêtes. Je veux maintenant l'ouvrir à l'extérieur par l'intermédiaire d'un **reverse proxy**. Il en existe quelques uns très populaire en ce moment qui font le boulot. Pour cet exercice nous avons choisi d'utiliser [Nginx](https://www.nginx.com/).

1. Explorer le dossier `/etc/nginx/sites-available`. Y créer un fichier de configuration avec le nom `prenom.caracals.org`.

2. Vous inspirez des autres fichiers présents pour écrire votre fichier. Attention, `sudo` est necessaire pour la modification de ce dossier. Laisser les parties `#certbot`

3. Créer un lien symbolique pour activer votre configuration. `sudo ln -s /etc/nginx/sites-availables/prenom.caracals.org /etc/nginx/sites-enabled/prenom.caracals.org`.

4. On ajoute rapidement un [certificat de sécurité](https://fr.wikipedia.org/wiki/Certificat_%C3%A9lectronique) pour assurer les échanges en https avec l'outil [CertBot](https://certbot.eff.org/) de l'[EFF](https://www.eff.org/). Lancer la commande `sudo certbot --nginx`. Suivre la console pour sécuriser votre nom de domaine.

5. Relancer le service nginx avec la commande `sudo nginx -s reload`.

6. Depuis votre ordinateur se rendre sur `https://prenom.caracals.org`.

Voilà, vous venez d'auto héberger votre premier site web :D