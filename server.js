const express = require('express');
const process = require('process');

const app = express();
const port = process.env.PORT || 3000;
// Si la variable d'environnement n'est pas configuré, démarrage par défaut sur le port 3000

app.get('/', (req, res) => {
    res.send('Bien joué, tu as réussi. Ce serveur répond aux requêtes !');
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})
